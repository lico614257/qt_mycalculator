#include "widget.h"

#include <QApplication>
#include <QtDebug>

int main(int argc, char *argv[])
{

    QApplication a(argc, argv);
    Widget w;
    w.setFixedSize(435,300);
    w.show();
    return a.exec();
}
