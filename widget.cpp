#include "widget.h"
#include "ui_widget.h"
#include <QDebug>

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);

    QFont font;
    font.setPointSize(10); //字号大小
    font.setFamily(("wenquanyi")); //字体样式
    font.setBold(false);
    ui->lineEdit_show->setFont(font); // 为控件设置格式

    m_temp.clear();
    m_list.clear();
}

Widget::~Widget()
{
    delete ui;
}

// number 0
void Widget::on_pbt_0_clicked()
{
    m_temp.append("0");
    ui->lineEdit_show->setText(m_temp);
}
//number 1
void Widget::on_pbt_1_clicked()
{
    m_temp.append("1");
    ui->lineEdit_show->setText(m_temp);
}
// number 2
void Widget::on_pbt_2_clicked()
{
    m_temp.append("2");
    ui->lineEdit_show->setText(m_temp);
}
// number 3
void Widget::on_pbt_3_clicked()
{
    m_temp.append("3");
    ui->lineEdit_show->setText(m_temp);
}
// nmber 4
void Widget::on_pbt_4_clicked()
{
    m_temp.append("4");
    ui->lineEdit_show->setText(m_temp);
}
// number 5
void Widget::on_pbt_5_clicked()
{
    m_temp.append("5");
    ui->lineEdit_show->setText(m_temp);
}
// number 6
void Widget::on_pbt_6_clicked()
{
    m_temp.append("6");
    ui->lineEdit_show->setText(m_temp);
}
//number 7
void Widget::on_pbt_7_clicked()
{
    m_temp.append("7");
    ui->lineEdit_show->setText(m_temp);
}
// number 8
void Widget::on_pbt_8_clicked()
{
    m_temp.append("8");
    ui->lineEdit_show->setText(m_temp);
}
// number 9
void Widget::on_pbt_9_clicked()
{
    m_temp.append("9");
    ui->lineEdit_show->setText(m_temp);
}
// operator +
void Widget::on_opetator_plus_clicked()
{
    m_list.insert(0,m_temp);
    m_temp.clear();
    m_list.insert(1,"+");
    ui->lineEdit_show->setText(m_temp);
}
// operator -
void Widget::on_opetator_sub_clicked()
{
    m_list.insert(0,m_temp);
    m_temp.clear();
    m_list.insert(1,"-");
    ui->lineEdit_show->setText(m_temp);
}
// operator *
void Widget::on_opetator_multi_clicked()
{
    m_list.insert(0, m_temp);
    m_temp.clear();
    m_list.insert(1, "*");
    ui->lineEdit_show->setText(m_temp);
}
// operator /
void Widget::on_opetator_divi_clicked()
{
    m_list.insert(0, m_temp);
    m_temp.clear();
    m_list.insert(1, "/");
    ui->lineEdit_show->setText(m_temp);
}
// operator =
void Widget::on_opetator_result_clicked()
{
    if(m_list.count() == 2){
        m_list.insert(2, m_temp);

//        qDebug() << m_list.at(0) <<endl;
//        qDebug() << m_list.at(1) <<endl;
//        qDebug() << m_list.at(2) <<endl;

        if(m_list.at(0).contains(".") || m_list.at(1).contains(".")){
            double res = 0.0;
            if(m_list.at(1) == "+"){
                res = QString(m_list.at(0)).toDouble() + QString(m_list.at(2)).toDouble();
            }
            else if(m_list.at(1) == "-"){
                res = QString(m_list.at(0)).toDouble() - QString(m_list.at(2)).toDouble();

            }
            else if(m_list.at(1) == "*"){
                res = QString(m_list.at(0)).toDouble() * QString(m_list.at(2)).toDouble();
            }
            else if(m_list.at(1) == "/"){
                if(QString(m_list.at(2)).toDouble() == 0){
                    res = 0;
                }
                else{
                    res = QString(m_list.at(0)).toDouble() / QString(m_list.at(2)).toDouble();
                }
            }
            ui->lineEdit_show->setText(QString::number(res));
        }
        else{
            int res = 0;
            if(m_list.at(1) == "+"){
                res = QString(m_list.at(0)).toInt() + QString(m_list.at(2)).toInt();
            }
            else if(m_list.at(1) == "-"){
                res = QString(m_list.at(0)).toInt() - QString(m_list.at(2)).toInt();

            }
            else if(m_list.at(1) == "*"){
                res = QString(m_list.at(0)).toInt() * QString(m_list.at(2)).toInt();
            }
            else if(m_list.at(1) == "/"){
                if(QString(m_list.at(2)).toInt() == 0.){
                    res = 0;
                }
                else{
                    res = QString(m_list.at(0)).toInt() / QString(m_list.at(2)).toInt();
                }
            }
            ui->lineEdit_show->setText(QString::number(res));
        }


    }
    else{
        m_temp.clear();
        m_list.clear();
        ui->lineEdit_show->setText(m_temp);
    }
}
// operator <-
void Widget::on_opetator_del_clicked()
{
    m_temp.chop(1);   //chop(int n) 倒位剔除函数
    ui->lineEdit_show->setText(m_temp);
}
// operator C
void Widget::on_opetator_cancel_clicked()
{
    ui->lineEdit_show->setText("");
    m_temp.clear();
    m_list.clear();
}
// operator .  小数运算
void Widget::on_opetator_point_clicked()
{
    m_temp.append(".");
    ui->lineEdit_show->setText(m_temp);
}
// operator +/-
void Widget::on_opetator_PtoN_clicked()
{
    m_temp = QString::number(m_temp.toInt() * (-1));
    ui->lineEdit_show->setText(m_temp);
}
