#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();

private slots:
    void on_pbt_0_clicked();

    void on_pbt_1_clicked();

    void on_pbt_2_clicked();

    void on_pbt_3_clicked();

    void on_pbt_4_clicked();

    void on_pbt_5_clicked();

    void on_pbt_6_clicked();

    void on_pbt_7_clicked();

    void on_pbt_8_clicked();

    void on_pbt_9_clicked();

    void on_opetator_plus_clicked();

    void on_opetator_sub_clicked();

    void on_opetator_multi_clicked();

    void on_opetator_divi_clicked();

    void on_opetator_result_clicked();

    void on_opetator_del_clicked();

    void on_opetator_cancel_clicked();

    void on_opetator_point_clicked();

    void on_opetator_PtoN_clicked();

private:
    Ui::Widget *ui;

    QString m_temp;
    QStringList m_list;

};
#endif // WIDGET_H
